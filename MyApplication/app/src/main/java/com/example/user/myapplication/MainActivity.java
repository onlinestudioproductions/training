package com.example.user.myapplication;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.user.core.utils.JSONUtils;
import com.example.user.myapplication.model.LoginRaw;
import com.example.user.myapplication.utils.CustomDialog;
import com.example.user.core.view.BaseActionBarActivity;
import com.example.user.myapplication.utils.DemoConstants;
import com.example.user.myapplication.utils.LogUtils;

import org.json.JSONObject;

import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class MainActivity extends BaseActionBarActivity {

    private  static  final  String TAG= "MainActivity";

   // private Button butLogin;
    @InjectView(R.id.butLogin) View butLogin;
    @InjectView(R.id.eteUser) EditText eteUser;
    @InjectView(R.id.etePassword) EditText etePassword;
    @InjectView(R.id.rlayLoading) View rlayLoading;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        showActionbar(false);
        ButterKnife.inject(this);
        initView();
    }

    public void initView(){
       // butLogin = (Button)findViewById(R.id.butLogin);
        rlayLoading.setVisibility(View.GONE);
        butLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LogUtils.LOGI(TAG, "onClick ");
                /*if(validateForm())
                {
                    login();
                }*/
                String user = eteUser.getText().toString().trim();//((EditText)findViewById(R.id.eteUser)).getText().toString();
                String password = etePassword.getText().toString().trim();//((EditText)findViewById(R.id.etePassword)).getText().toString();
                validateLogin(user,password);
            }
        });

    }
    
    
    public void validateLogin(String user, String password){

        eteUser.setError(null);
        etePassword.setError(null);

        if(!user.isEmpty() && !password.isEmpty()){

            login();
        }else
        {
            if(user.isEmpty())
            {
                eteUser.setError("Error en este campo");
                eteUser.requestFocus();
            }
            if(password.isEmpty())
            {
                etePassword.setError("Error en este campo");
                etePassword.requestFocus();
            }

            // etePassword.setError("Error en este campo");
            //etePassword.requestFocus();
            CustomDialog.crearSimpleAlertDialog(MainActivity.this, getString(R.string.app_name), getString(R.string.sEmpyUserPasswor)).show();
        }
        

    }

    private void login() {
        //rlayLoading.setVisibility(View.VISIBLE);
        LoginRaw loginRaw = new LoginRaw();
        loginRaw.setUsername(getString(R.string.tmp_username));//eteCorreoSesion.getText().toString());
        loginRaw.setPassword(getString(R.string.tmp_password));//etePasswordSesion.getText().toString());

        requestPOST_INICIAR_SESION(loginRaw);
    }
    public void requestPOST_INICIAR_SESION(LoginRaw loginRaw)
    {
        rlayLoading.setVisibility(View.VISIBLE);
        RequestQueue queue = Volley.newRequestQueue(this);
        //Log.i("LOGIN", JSON.generarJSONObject(loginRaw).toString());
        LogUtils.LOGI(TAG,"loginRaw "+loginRaw.toString());

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, getResources().getString(R.string.url_login_usuario), JSONUtils.generateJSONObject(loginRaw),
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtils.LOGI(TAG, "Response "+response.toString());
                        rlayLoading.setVisibility(View.GONE);
                        try{

                            JSONObject jsonObject = response.getJSONObject("user");
                            boolean esActivo = jsonObject.getBoolean("is_active");

                            if(esActivo){
                                nextActivity(HomeActivity.class,true);
                            }

                        }catch (Exception e){
                            e.printStackTrace();
                            //Error
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                LogUtils.LOGI(TAG, "Error " + error);
                rlayLoading.setVisibility(View.GONE);
                // Error
            }
        })
        {
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {

                Map headers = response.headers;
                String cookie = (String) headers.get("Set-Cookie");

                //CunucSharedPreferences.saveCookie(getActivity(),cookie);
                LogUtils.LOGI(TAG,"cookie "+cookie);

                return super.parseNetworkResponse(response);
            }
        };

        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(DemoConstants.TIMEOUT, 1,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(jsObjRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        //return true;
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        /*int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);*/
        return false;
    }
}

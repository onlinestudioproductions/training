package com.example.user.core.view;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.widget.TextView;

/**
 * Created by emedinaa on 24/10/14.
 */
public class BaseActionBarActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected  void nextActivity(Class<?> activity)
    {
        nextActivity(activity,false);
    }
    protected void nextData(Class<?> activity,Bundle bundle)
    {
        nextData(activity,bundle,false);
    }

    protected  void nextActivity(Class<?> activity,boolean notDestroy)
    {
        startActivity(new Intent(this, activity));
        if(!notDestroy)finish();
    }

    protected void nextData(Class<?> activity,Bundle bundle,boolean notDestroy)
    {
        Intent intent=new Intent(this, activity);
        intent.putExtras(bundle);
        startActivity(intent);
        if(!notDestroy)finish();
    }

    protected void showActionbar(boolean mstate)
    {
        if(mstate)
        {
            getSupportActionBar().show();
        }else
        {
            getSupportActionBar().hide();
        }
    }

    protected void startCustomActionBar(int mCustomView)
    {
        ActionBar mActionBar = getSupportActionBar();
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);
       //getSupportActionBar().setCustomView(xml);
        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        showActionbar(true);
    }

    public void setCustomFont(TextView txtview,String font,int style)
    {
        //http://mobile.tutsplus.com/tutorials/android/customize-android-fonts/
        Typeface typefaceFont = Typeface.createFromAsset(getAssets(), font);
        if(style==-1)
        {
            txtview.setTypeface(typefaceFont);
        }else
        {
            txtview.setTypeface(typefaceFont,style);
        }
    }
}

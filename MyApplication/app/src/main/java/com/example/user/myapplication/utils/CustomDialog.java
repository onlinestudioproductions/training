package com.example.user.myapplication.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.example.user.myapplication.R;

/**
 * Created by jebus on 25/11/14.
 */
public class CustomDialog {


    public static Dialog crearSimpleAlertDialog(Context context, String titulo, String mensaje){

        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setPositiveButton(context.getResources().getString(R.string.sAceptar), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

        View view = View.inflate(context, R.layout.alert_mensaje, null);
        TextView tviTitulo = (TextView) view.findViewById(R.id.tviTitulo);
        TextView tviMensaje = (TextView) view.findViewById(R.id.tviMensaje);

        tviTitulo.setText(titulo);
        tviMensaje.setText(mensaje);

        builder.setView(view);
        return  builder.create();
    }

    public static Dialog crearSimpleAlertDialogIntentAceptar(final Context context, String titulo, String mensaje, final Intent intent){

        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setPositiveButton(context.getResources().getString(R.string.sAceptar), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        context.startActivity(intent);
                    }
                });

        View view = View.inflate(context, R.layout.alert_mensaje, null);
        TextView tviTitulo = (TextView) view.findViewById(R.id.tviTitulo);
        TextView tviMensaje = (TextView) view.findViewById(R.id.tviMensaje);

        tviTitulo.setText(titulo);
        tviMensaje.setText(mensaje);

        builder.setView(view);
        return  builder.create();
    }
}

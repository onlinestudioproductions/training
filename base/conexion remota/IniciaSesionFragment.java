package com.osp.projects.appcunuc.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.osp.projects.core.util.CustomDialog;
import com.osp.projects.core.util.JSON;
import com.osp.projects.core.util.LogUtils;
import com.osp.projects.core.util.SesionUsuarioBorrar;
import com.osp.projects.core.util.TypefaceText;
import com.osp.projects.appcunuc.ContentActivity;
import com.osp.projects.appcunuc.R;
import com.osp.projects.appcunuc.model.Usuario;
import com.osp.projects.appcunuc.model.entityRaw.LoginRaw;
import com.osp.projects.appcunuc.storage.CunucSharedPreferences;
import com.osp.projects.appcunuc.utils.CunucConst;

import org.json.JSONObject;

import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class IniciaSesionFragment extends Fragment implements View.OnClickListener{

    private TextView tviCorreo, tviContraseña;
    private EditText eteCorreoSesion, etePasswordSesion;
    private RelativeLayout rlaCargando;

    @InjectView(R.id.buttonIniciaSesion)
    Button buttonIniciaSesion;


    // TODO: Rename and change types and number of parameters
    public static IniciaSesionFragment newInstance(RelativeLayout rlaCargando) {
        IniciaSesionFragment fragment = new IniciaSesionFragment();
        fragment.rlaCargando = rlaCargando;
        return fragment;
    }
    public IniciaSesionFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_inicia_sesion, container, false);
        ButterKnife.inject(this,view);

       // initComponents(view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initComponents(getView());
        loadData();
    }

    private void initComponents(View view)
    {
       // buttonIniciaSesion = (Button) view.findViewById(R.id.buttonIniciaSesion);
        tviCorreo = (TextView) view.findViewById(R.id.tviCorreo);
        tviContraseña = (TextView) view.findViewById(R.id.tviContraseña);

        eteCorreoSesion = (EditText) view.findViewById(R.id.eteCorreoSesion);
        etePasswordSesion = (EditText) view.findViewById(R.id.etePasswordSesion);

       // buttonIniciaSesion.setOnClickListener(this);
        buttonIniciaSesion.setOnClickListener(this);
    }

    private void loadData()
    {
        new TypefaceText(getActivity()).cambiarTipoTexto("Avenir55Roman.ttf", tviCorreo);
        new TypefaceText(getActivity()).cambiarTipoTexto("Avenir55Roman.ttf", tviContraseña);
    }

    @Override
    public void onClick(View view) {

        if(buttonIniciaSesion == view) {
            if(eteCorreoSesion.getText().toString().compareTo("") == 0){
                CustomDialog.crearSimpleAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.mensaje_ingrese_correo)).show();
                return;
            }

            if(etePasswordSesion.getText().toString().compareTo("") == 0){
                CustomDialog.crearSimpleAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.mensaje_ingrese_contrasena)).show();
                return;
            }

            LoginRaw loginRaw = new LoginRaw();
            loginRaw.setUsername(getString(R.string.tmp_username));//eteCorreoSesion.getText().toString());
            loginRaw.setPassword(getString(R.string.tmp_password));//etePasswordSesion.getText().toString());

            requestPOST_INICIAR_SESION(loginRaw);
            /*
            Intent intent = new Intent(getActivity(), ContentActivity.class);
            CustomDialog.crearSimpleAlertDialogIntentAceptar(getActivity(), getString(R.string.app_name), getString(R.string.mensaje_sesion_correcta), intent).show();
            */
        }
    }

    public void requestPOST_INICIAR_SESION(LoginRaw loginRaw)
    {
        rlaCargando.setVisibility(View.VISIBLE);
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        //Log.i("LOGIN", JSON.generarJSONObject(loginRaw).toString());
        LogUtils.LOGI("IniciaSesionFragment","loginRaw "+loginRaw.toString());

        JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, getResources().getString(R.string.url_login_usuario), JSON.generarJSONObject(loginRaw),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        LogUtils.LOGI("IniciaSesionFragment", "Response "+response.toString());
                        rlaCargando.setVisibility(View.GONE);
                        try{
                            //GsonBuilder gsonb = new GsonBuilder();
                            //Gson gson = gsonb.create();
                            //LoginResponse responseRequest = gson.fromJson(response.toString(), LoginResponse.class);

                            JSONObject jsonObject = response.getJSONObject("user");
                            boolean esActivo = jsonObject.getBoolean("is_active");

                            if(esActivo){
                                Usuario usuario = new Usuario();
                                usuario.setId(jsonObject.getInt("id"));
                                SesionUsuarioBorrar.guardarSesionUsuario(getActivity(), usuario, true);
                                Intent intent = new Intent(getActivity(), ContentActivity.class);
                                startActivity(intent);
                                //CustomDialog.crearSimpleAlertDialogIntentAceptar(getActivity(), getString(R.string.app_name), getString(R.string.mensaje_sesion_correcta), intent).show();
                            }

                        }catch (Exception e){
                            e.printStackTrace();
                            CustomDialog.crearSimpleAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.mensaje_problema_sesion)).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LogUtils.LOGI("IniciaSesionFragment", "Error " + error);
                rlaCargando.setVisibility(View.GONE);
                CustomDialog.crearSimpleAlertDialog(getActivity(), getString(R.string.app_name), getString(R.string.mensaje_problema_sesion)).show();
            }
        })
        {
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {

                Map headers = response.headers;
                String cookie = (String) headers.get("Set-Cookie");
                CunucSharedPreferences.saveCookie(getActivity(),cookie);
                LogUtils.LOGI("INICIARSESIONFRAGMENT","cookie "+cookie);

                return super.parseNetworkResponse(response);
            }
        }
/*
        {
            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                // since we don't know which of the two underlying network vehicles
                // will Volley use, we have to handle and store session cookies manually
                MyApp.get().checkSessionCookie(response.headers);

                return super.parseNetworkResponse(response);
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = super.getHeaders();

                if (headers == null || headers.equals(Collections.emptyMap())) {
                    headers = new HashMap<String, String>();
                }

                MyApp.get().addSessionCookie(headers);

                return headers;
            }

        }
        */;
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(CunucConst.TIMEOUT, 1,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(jsObjRequest);
    }
}

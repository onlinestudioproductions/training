package com.osp.projects.appcunuc.model.entityRaw;

import java.io.Serializable;

/**
 * Created by emedinaa on 10/12/14.
 */
public class LoginRaw implements Serializable {

    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "LoginRaw{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
